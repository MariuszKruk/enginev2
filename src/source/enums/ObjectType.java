package source.enums;

public enum ObjectType{
    Player(),
    Ball(),
    Particle(),
    Coin()
}
