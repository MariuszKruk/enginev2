package source.otherRender;

import java.awt.*;

public class HUD{

    private static int MAXHEALTH = 100;
    public static int HEALTH = 100;
    public static int MONEY = 0;

    private static boolean isHited = true;

    public void tick(){

    }
    public void render(Graphics g){
        g.setColor(Color.darkGray);
        g.fillRect(7,7,150,16);
        createBorder(g,7, 7 ,150, 16, 2);
        g.setColor(Color.green);
        g.fillRect(7,7,150*(HEALTH/MAXHEALTH),16);
        createBorder(g,175, 7 ,16, 16, 2);
        g.setColor(Color.yellow);
        g.fillRect(175,7,16,16);
        g.setColor(Color.black);
        g.drawString("$",180,19);
        g.setColor(Color.yellow);
        g.drawString(Integer.toString(MONEY),195,19);
    }
    public void createBorder(Graphics g, int x, int y, int width, int height, int size){
        int sizel=2*size;
        g.setColor(Color.WHITE);
        g.fillRect(x-size,y-size,width+sizel,height+sizel);
    }

    public static void setHEALTH(int health){
        HEALTH=health;
    }

    public static void setMONEY(int money){
        MONEY=money;
    }
}
