package source.otherRender.scenes;

import source.devTools.DIDevConsole;
import source.devTools.DevConsole;
import source.engine.DIWindow;
import source.engine.Engine;
import source.engine.Window;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Menu extends MouseAdapter implements DIDevConsole, DIWindow{
    Engine engine;
    Window window;
    DevConsole devConsole;

    private int buttonwidth;
    private int buttonheight;
    private int buttonxpos;
    private int buttonypos;

    public Menu(Engine engine){
        this.engine = engine;



        System.out.println(devConsole);

        setButtonsValues();
    }

    @Override
    public void setDevConsole(DevConsole devConsole) {
        this.devConsole = devConsole;
    }

    @Override
    public void setWindow(Window window){
        this.window = window;

    }
    public void mousePressed(MouseEvent e){
        int mx = e.getX();
        int my = e.getY();
        if(engine.getState()== Engine.State.Menu){
            if (mouseOver(mx, my, buttonxpos, buttonypos, buttonwidth, buttonheight)) {
                engine.setState(Engine.State.Work);
            } else if (mouseOver(mx, my, buttonxpos, buttonypos, buttonwidth, buttonheight + 85)) {
                engine.setState(Engine.State.Options);
            } else if (mouseOver(mx, my, buttonxpos, buttonypos, buttonwidth, buttonheight + 170)) {
                engine.injectWindow(this);
                engine.injectDevConsole(this);

                engine.suspend();
                devConsole.close();
                window.close();
                engine = null;
            }
        }
    }
    public void mouseReleased(MouseEvent e){

    }
    public boolean mouseOver(int mx,int my,int x,int y,int width,int height){
        if(mx > x && mx < x + width){
            if(my > y && my < y + height){
                return true;
            }else return false;
        }else return false;
    }

    public void render(Graphics g){
        renderTitle(g);
        renderButton(g,"Continue",0,0);
        renderButton(g, "Options", 85, 30);
        renderButton(g, "Exit", 170, 30);


        g.setColor(Color.WHITE);
        //g.setFont(font);

    }
    public void tick(){

    }
    public void setButtonsValues(){
        buttonwidth = 20*engine.getScale();
        buttonheight = 4*engine.getScale();
        buttonxpos = engine.getWidth()/2-buttonwidth/2;
        buttonypos = 200;

    }
    public void renderButton(Graphics g,String buttonstring, int rectshifty, int stringshiftx){
        Font font = new Font("ethnocentric",0,30);
        g.setColor(new Color(23, 23, 30));
        g.fillRect(buttonxpos, buttonypos + rectshifty, buttonwidth, buttonheight);
        g.setColor(Color.white);
        g.setFont(font);
        g.drawString(buttonstring,engine.getWidth()/2-buttonwidth/2+40+stringshiftx,buttonypos+32+rectshifty);
        g.setColor(Color.white);
        g.drawRect(buttonxpos, buttonypos + rectshifty, buttonwidth, buttonheight);
    }
    public void renderTitle(Graphics g){
        g.setFont(new Font("arial", 0,60));
        g.setColor(new Color(255, 212, 0));
        g.drawString(engine.getTitle(), engine.getWidth()/2-87,90);
    }
}
