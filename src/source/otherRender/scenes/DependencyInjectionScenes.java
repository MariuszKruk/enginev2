package source.otherRender.scenes;

/**
 * Created by Kuba on 2018-05-06.
 */
public interface DependencyInjectionScenes {
    void setMenu(Menu menu);
    void setStart(Start start);
    void setLoading(Loading loading);
    void setIntro(Intro intro);
    void setOptions(Options options);

}
