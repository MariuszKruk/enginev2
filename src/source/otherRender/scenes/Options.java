package source.otherRender.scenes;

import source.engine.Engine;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created by Kuba on 2018-05-07.
 */
public class Options extends MouseAdapter{

    private final Engine engine;

    private int buttonwidth;
    private int buttonheight;
    private int buttonxpos;
    private int buttonypos;

    private int checkboxsize;
    private int checkboxxpos;
    private int checkboxypos;


    public Options(Engine engine){
        this.engine = engine;
        setButtonsValues();
        setCheckBoxValues();
    }

    public void mousePressed(MouseEvent e){
        int mx = e.getX();
        int my = e.getY();

        if(mouseOver(mx, my, 0,0,0,0)){

        }
    }
    public void mouseReleased(MouseEvent e){

    }
    public boolean mouseOver(int mx,int my,int x,int y,int width,int height){
        if(mx > x && mx < x + width){
            if(my > y && my < y + height){
                return true;
            }else return false;
        }else return false;
    }

    public void render(Graphics g){
        renderCheckBox(g, "name",0,0);

    }
    public void tick(){

    }
    public void setButtonsValues(){
        buttonwidth = 20*engine.getScale();
        buttonheight = 4*engine.getScale();
        buttonxpos = engine.getWidth()/2-buttonwidth/2;
        buttonypos = 200;

    }
    public void setCheckBoxValues(){
        double checkboxsize0 = (1.5*engine.getScale());
        checkboxsize = (int) checkboxsize0;
        buttonxpos = engine.getWidth()/2-buttonwidth/2;
        buttonypos = 200;

    }
    public void renderButton(Graphics g, String buttonstring, int rectshifty, int stringshiftx){
        Font font = new Font("ethnocentric",0,30);
        g.setColor(new Color(23, 23, 30));
        g.fillRect(buttonxpos, buttonypos + rectshifty, buttonwidth, buttonheight);
        g.setColor(Color.white);
        g.setFont(font);
        g.drawString(buttonstring,engine.getWidth()/2-buttonwidth/2+40+stringshiftx,buttonypos+32+rectshifty);
        g.setColor(Color.white);
        g.drawRect(buttonxpos, buttonypos + rectshifty, buttonwidth, buttonheight);
    }
    public void renderCheckBox(Graphics g, String boxstring ,int boxshiftx, int boxshifty){
        g.setColor(new Color(23,23,30));
        g.fillRect(checkboxxpos + boxshiftx, checkboxypos + boxshifty, checkboxsize, checkboxsize);
        g.setColor(Color.WHITE);
        g.drawRect(checkboxxpos + boxshiftx, checkboxypos + boxshifty, checkboxsize, checkboxsize);
    }
}
