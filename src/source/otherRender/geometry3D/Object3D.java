package source.otherRender.geometry3D;

/**
 * Created by Kuba on 2018-05-11.
 */
public abstract class Object3D{

    protected int x,y,z;

    public int getX(){
        return x;
    }
    public int getY(){
        return y;
    }
    public int getZ(){
        return z;
    }
    public void setX(int x){
        this.x = x;
    }
    public void setY(int y){
        this.y = y;
    }
    public void setZ(int z){
        this.z = z;
    }
    public void setLocation(int x, int y, int z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

}
