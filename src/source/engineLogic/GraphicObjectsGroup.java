package source.engineLogic;

import source.engine.Engine;
import source.enums.GroupObjectType;
import source.enums.ObjectType;

import java.awt.*;
import java.util.Random;

public abstract class GraphicObjectsGroup {

    protected int x[]= new int[5], y[];
    protected int xSize[], ySize[];
    protected int xVel[], yVel[];

    protected String internalUID[];
    protected String internalName[];
    protected ObjectType internalObjectType[];

    protected GroupObjectType groupObjectType;
    protected String uID;
    protected String name;
    protected int objectsQuatity;

    protected Handler handler;
    protected Engine engine;

    public GraphicObjectsGroup(int objectsQuatity, String name, GroupObjectType groupObjectType, int x[], int y[], int xSize[], int ySize[], String internalName[], Handler handler, Engine engine){
        this.handler = handler;
        this.engine = engine;

        this.internalName = new String[objectsQuatity];
        this.internalUID= new String[objectsQuatity];
        this.internalObjectType = new ObjectType[objectsQuatity];

        this.x=x;
        this.y=y;
        this.name=name;
        this.uID = GenerateUID();
        this.groupObjectType=groupObjectType;
        this.objectsQuatity=objectsQuatity;
        this.internalName=internalName;
        this.xSize=xSize;
        this.ySize=ySize;

        for(int c = 0;c<objectsQuatity;c++){
            this.internalUID[c] = GenerateUID();
        }
    }

    public GraphicObjectsGroup(int objectsQuatity, String name, GroupObjectType groupObjectType ,int x[], int y[], int xSize, int ySize, String internalName, Handler handler, Engine engine){
        this.handler = handler;
        this.engine = engine;

        this.objectsQuatity=objectsQuatity;
        this.xSize = new int[objectsQuatity];
        this.ySize = new int[objectsQuatity];
        this.internalName = new String[objectsQuatity];
        this.internalUID= new String[objectsQuatity];
        this.internalObjectType = new ObjectType[objectsQuatity];

        this.name=name;
        this.groupObjectType=groupObjectType;
        this.uID=GenerateUID();
        this.x=x;
        this.y=y;

        for(int c = 0;c<objectsQuatity;c++){
            this.xSize[c]=xSize;
            this.ySize[c]=ySize;
            this.internalUID[c] = GenerateUID();
            this.internalName[c]= internalName;
        }
    }

    public GraphicObjectsGroup(int objectsQuatity, String name, GroupObjectType groupObjectType, int xSize, int ySize, String internalName, Handler handler, Engine engine){
        Random random = new Random();

        this.handler = handler;
        this.engine = engine;

        this.internalName = new String[objectsQuatity];
        this.internalUID = new String[objectsQuatity];
        this.internalObjectType = new ObjectType[objectsQuatity];
        this.xSize = new int[objectsQuatity];
        this.ySize = new int[objectsQuatity];
        this.x = new int[objectsQuatity];
        this.y = new int[objectsQuatity];

        this.name=name;
        this.uID = GenerateUID();
        this.groupObjectType=groupObjectType;
        this.objectsQuatity=objectsQuatity;

        for(int c = 0;c<objectsQuatity;c++){
            this.xSize[c]=xSize;
            this.ySize[c]=ySize;
            this.internalUID[c] = GenerateUID();
            this.internalName[c]= internalName;
            this.x[c]=random.nextInt(engine.getWidth());
            this.y[c]=random.nextInt(engine.getHeigh());
        }
    }
    
    public String getIdentity(){
        return "Name:["+name+"] ID:["+ID"] ObjectType:["+internalObjectType+"] GroupObjectType:["+groupObjectType+"]";
    }

    public abstract void tick();
    public abstract void render(Graphics g);
    public String GenerateUID(){
        String id = "["+groupObjectType.toString()+"]:";
        char genID[] = {'A','B','C','D','Q','W','E','T','Y','U','I','R','P','K','J','H','G','S','X','Z','V','1','2','3','4','5','6','7','8','9','0'};
        Random rand = new Random();
        for (int I = 8; I > 0; I--) {
            int index = rand.nextInt(31);
            id = id + genID[index];
        }
        System.out.println("id: "+id);
        return id;
    }

    public GroupObjectType getGroupObjectType(){
        return groupObjectType;
    }
    public String getName(){
        return name;
    }
    public String getId(){
        return uID;
    }

}
