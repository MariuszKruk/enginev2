package source.engineLogic;

public class Tasks{

    private long time[]={0,0,0,0,0};

    Handler handler;

    public Tasks(Handler handler){
        this.handler = handler;
        time[1]=System.currentTimeMillis();
        time[2]=System.currentTimeMillis();
    }

    //LOOPS

    public void secondLoop(){
        if(time[3]-time[2]>1000){
            time[2]=System.currentTimeMillis();
            //code
        }
        time[3]=System.currentTimeMillis();
    }
    public void minuteLoop(){
        if(time[4]-time[1]>60000){
            time[1]=System.currentTimeMillis();
            //code
        }
        time[4]=System.currentTimeMillis();
    }

    public void tick(){
        secondLoop();
        minuteLoop();
    }

    //PLANNED TASKS

    //TASKS
}
