package source.engineLogic;

import source.enums.GroupObjectType;
import source.enums.ObjectType;

import java.awt.*;
import java.util.LinkedList;

public class Handler{

    public final LinkedList<GraphicObject> graphicObjects = new LinkedList<>();
    public final LinkedList<LogicObject> logicObjects = new LinkedList<>();
    public final LinkedList<GraphicObjectsGroup> graphicObjectsGroups = new LinkedList<>();

    public void tick(){
        for(int i = 0; i < graphicObjects.size(); i++){
            GraphicObject tempObject = graphicObjects.get(i);
            tempObject.tick();
        }
        for(int i = 0; i < graphicObjectsGroups.size(); i++){
            GraphicObjectsGroup tempObject = graphicObjectsGroups.get(i);
            tempObject.tick();
        }
        for(int i = 0; i < logicObjects.size(); i++){
            LogicObject logicObject = logicObjects.get(i);
            logicObject.tick();
        }
    }

    public void render(Graphics g){
        for(int i =0; i < graphicObjects.size(); i++){
            GraphicObject tempObject = graphicObjects.get(i);
            tempObject.render(g);
            if(tempObject.getObjectType()==ObjectType.Player){
                graphicObjects.remove(tempObject);
                graphicObjects.addLast(tempObject);
            }
        }
        for(int i =0; i < graphicObjectsGroups.size(); i++){
            GraphicObjectsGroup tempObject = graphicObjectsGroups.get(i);
            tempObject.render(g);
        }
        for(int i = 0; i < logicObjects.size(); i++){
            LogicObject logicObject = logicObjects.get(i);
            logicObject.render(g);
        }
    }
    public void addObject(GraphicObject object){
        this.graphicObjects.add(object);
    }

    public void addGraphicObjectGroup(GraphicObjectsGroup graphicObjectsGroup){
        this.graphicObjectsGroups.add(graphicObjectsGroup);
    }

    public void addLogicObject(LogicObject logicObject){
        logicObjects.add(logicObject);
    }

    public int objectsCount(ObjectType objectType){
        int quantity=0;
        for(int i =0;i< graphicObjects.size();i++){
            GraphicObject tempobject = graphicObjects.get(i);
            if(tempobject.getObjectType()== objectType){
                quantity++;
            }
        }
        return quantity;
    }

    public void removeObject(GraphicObject object){
        this.graphicObjects.remove(object);
    }

    public void removeObjectByName(String name){
        for(int i =0;i< graphicObjects.size();i++){
            GraphicObject tempobject = graphicObjects.get(i);
            if(tempobject.getName().equals(name)){
                graphicObjects.remove(tempobject);
            }
        }
    }

    public void removeObjectById(String id){
        for(int i =0;i< graphicObjects.size();i++){
            GraphicObject tempobject = graphicObjects.get(i);
            if(tempobject.getID().equals(id)){
                graphicObjects.remove(tempobject);
                break;
            }
        }
    }

    public void removeObjectByType(ObjectType objectType){
        for(int i =0;i< graphicObjects.size();i++){
            GraphicObject tempobject = graphicObjects.get(i);
            if(tempobject.getObjectType()==objectType){
                graphicObjects.remove(tempobject);
            }
        }
    }

    public void removeObjectsGroupByType(GroupObjectType groupObjectType){
        for(int i = 0; i<graphicObjectsGroups.size();i++){
            GraphicObjectsGroup tempgraphicobjectsgroup = graphicObjectsGroups.get(i);
            if(tempgraphicobjectsgroup.getGroupObjectType()==groupObjectType){
                graphicObjectsGroups.remove(tempgraphicobjectsgroup);
            }
        }

    }
    public void removeObjectsGroupByName(String name){
        for(int i = 0; i<graphicObjectsGroups.size();i++){
            GraphicObjectsGroup tempgraphicobjectsgroup = graphicObjectsGroups.get(i);
            if(tempgraphicobjectsgroup.getName().equals(name)){
                graphicObjectsGroups.remove(tempgraphicobjectsgroup);
            }
        }
    }
    public void removeObjectsGroupById(String Id){
        for(int i = 0; i<graphicObjectsGroups.size();i++){
            GraphicObjectsGroup tempgraphicobjectsgroup = graphicObjectsGroups.get(i);
            if(tempgraphicobjectsgroup.getId().equals(Id)){
                graphicObjectsGroups.remove(tempgraphicobjectsgroup);
            }
        }
    }
    public void listAllObjects(){
        for(int i = 0; i < graphicObjects.size(); i++){
            GraphicObject tempGraphicObject = graphicObjects.get(i);
            System.out.println("Name:["+tempGraphicObject.getName()+"] Id:["+tempGraphicObject.getID()+"] Type:[GraphicObject:"+tempGraphicObject.getObjectType().toString()+"]");
        }
        for(int i = 0; i < graphicObjectsGroups.size(); i++){
            GraphicObjectsGroup tempGraphicObjectGroup = graphicObjectsGroups.get(i);
            System.out.println("Name:["+tempGraphicObjectGroup.getName()+"] Id:["+tempGraphicObjectGroup.getId()+"] Type:[GraphicObjectGroup:"+tempGraphicObjectGroup.getGroupObjectType().toString()+"]");
        }
        for(int i = 0; i < logicObjects.size(); i++){
            LogicObject templogicObject = logicObjects.get(i);
            System.out.println("Name:["+templogicObject.getName()+"] Id:["+templogicObject.getID()+"] Type:[LogicObject:"+templogicObject.getLogicObjectType().toString()+"]");
        }
    }

}
