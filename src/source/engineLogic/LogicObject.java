package source.engineLogic;

import source.engine.Engine;
import source.enums.LogicObjectType;

import java.awt.*;
import java.util.Random;

public abstract class LogicObject{
    protected int x, y;
    protected int xSize, ySize;

    protected String uID;
    protected String name;
    protected LogicObjectType logicObjectType;

    protected Handler handler;
    protected Engine engine;

    private boolean objectInfo = false;

    public LogicObject(int x, int y, int xSize, int ySize, LogicObjectType logicObjectType, String name, Handler handler, Engine engine, boolean objectInfo){
        this.x=x;
        this.y=y;
        this.name=name;
        this.xSize=xSize;
        this.ySize=ySize;
        this.engine=engine;
        this.handler=handler;
        this.objectInfo=objectInfo;
        this.logicObjectType=logicObjectType;
        uID=GenerateUID();
    }

    public LogicObject(int x, int y,int xSize,int ySize,LogicObjectType logicObjectType, String name, Handler handler, Engine engine){
        this.x=x;
        this.y=y;
        this.name=name;
        this.xSize=xSize;
        this.ySize=ySize;
        this.engine=engine;
        this.handler=handler;
        this.logicObjectType=logicObjectType;
        uID=GenerateUID();
    }

    public LogicObject(LogicObjectType logicObjectType, String name, Handler handler, Engine engine, boolean objectInfo){
        this.engine=engine;
        this.handler=handler;
        this.objectInfo=objectInfo;
        this.logicObjectType=logicObjectType;
        this.name=name;
        uID=GenerateUID();
    }

    public LogicObject(LogicObjectType logicObjectType, String name, Handler handler, Engine engine){
        this.name=name;
        this.engine=engine;
        this.handler=handler;
        this.logicObjectType=logicObjectType;
        uID=GenerateUID();
    }

    public abstract void tick();
    public abstract void render(Graphics g);
    public abstract Rectangle getBounds();

    public String getID(){
        return uID;
    }
    public String getName(){
        return name;
    }
    public LogicObjectType getLogicObjectType(){
        return logicObjectType;
    }


    public String GenerateUID(){
        String id = "["+logicObjectType.toString()+"]:";
        char genID[] = {'A','B','C','D','Q','W','E','T','Y','U','I','R','P','K','J','H','G','S','X','Z','V','1','2','3','4','5','6','7','8','9','0'};
        Random rand = new Random();
        for (int I = 8; I > 0; I--) {
            int index = rand.nextInt(31);
            id = id + genID[index];
        }
        System.out.println("id: " +id);
        return id;
    }
}
