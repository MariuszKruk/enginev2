package source.engineLogic;

import source.engine.Engine;
import source.enums.ObjectType;

import java.awt.*;
import java.util.Random;

public abstract class GraphicObject{


    /**
     * @param x Object Postion in x, y dimension
     * Velocity in x, y dimension
     * Size Height and Width of Graphic Object
     **/
    protected int x, y;
    protected int xSize, ySize;
    protected int xVel, yVel;

    /**
     * @param objectType ObjectType is variable which contains information about object type
     * @param uID is unique value for any instance of Graphic Object and can be used to veryfie specific object
     * @param name is name of specific object. Name is granted in constructors and can be multiplied. Has been created to simplify access and control under instance or instances(if name is equal for many instances).
     **/
    protected String uID;
    protected String name;
    protected ObjectType objectType;

    protected Handler handler;
    protected Engine engine;

    private boolean objectInfo = false;

    public GraphicObject(int x, int y, int xSize, int ySize, ObjectType objectType, String name, Handler handler, Engine engine, boolean objectInfo){
        this.x=x;
        this.y=y;
        this.name=name;
        this.xSize=xSize;
        this.ySize=ySize;
        this.engine=engine;
        this.handler=handler;
        this.objectInfo=objectInfo;
        this.objectType=objectType;
        uID=GenerateUID();
    }

    public GraphicObject(int x, int y,int xSize,int ySize,ObjectType objectType, String name, Handler handler, Engine engine){
        this.x=x;
        this.y=y;
        this.name=name;
        this.xSize=xSize;
        this.ySize=ySize;
        this.engine=engine;
        this.handler=handler;
        this.objectType=objectType;
        uID=GenerateUID();
    }

    public GraphicObject(ObjectType objectType, String name, Handler handler, Engine engine, boolean objectInfo){
        this.engine=engine;
        this.handler=handler;
        this.objectType=objectType;
        this.objectInfo=objectInfo;
        this.name=name;
        uID=GenerateUID();
    }

    public GraphicObject(ObjectType objectType, String name, Handler handler, Engine engine){
        this.engine=engine;
        this.handler=handler;
        this.objectType=objectType;
        this.name=name;
        uID=GenerateUID();
    }

    public abstract void tick();
    public abstract void render(Graphics g);
    public abstract Rectangle getBounds();
    public void clamp(int xPos, int xMax, int xMin, int yPos, int yMax, int yMin){
        x=xClamp(xPos, xMax, xMin);
        y=yClamp(yPos, yMax, yMin);
    }
    public int xClamp(int xPos, int xMax, int xMin){
        if(xPos>=xMax) {
            return xMax;
        }
        else if(xPos <=xMin) {
            return xMin;
        }
        else
            return xPos;

    }
    public int yClamp(int yPos, int yMax, int yMin){
        if(yPos>=yMax){
            return yMax;
        }
        else if(yPos<=yMin){
            return yMin;
        }
        else
            return yPos;

    }

    //SETERS

    public void setX(int x){
        this.x=x;
    }

    public void setY(int y){
        this.y=y;
    }

    public void setxVel(int xVel){
        this.xVel=xVel;
    }

    public void setyVel(int yVel){
        this.yVel=yVel;
    }

    public void setObjectType(ObjectType objectType){
        this.objectType=objectType;
    }

    //GETERS

    public ObjectType getObjectType(){
        return objectType;
    }

    public int getX(){
        return x;
    }

    public int getY(){
        return y;
    }

    public int getxSize(){
        return xSize;
    }

    public int getySize(){
        return ySize;
    }

    public int getxVel(){
        return xVel;
    }

    public int getyVel(){
        return yVel;
    }

    public String getName(){
        return name;
    }

    public String getID(){
        return uID;
    }

    public String GenerateUID(){
        String id ="["+objectType.toString()+"]:";
        char genID[] = {'A','B','C','D','Q','W','E','T','Y','U','I','R','P','K','J','H','G','S','X','Z','V','1','2','3','4','5','6','7','8','9','0'};
        Random rand = new Random();
        for (int I = 8; I > 0; I--) {
            int index = rand.nextInt(31);
            id = id + genID[index];
        }
        System.out.println("id: "+id);
        return id;
    }
}
