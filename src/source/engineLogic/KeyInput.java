package source.engineLogic;

import source.devTools.DevConsole;
import source.engine.Engine;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class KeyInput extends KeyAdapter{

    static boolean UP, DOWN, RIGHT, LEFT;

    private Handler handler;
    private DevConsole devConsole;
    private Engine engine;

    public KeyInput(Handler handler, DevConsole devConsole, Engine engine){
        this.handler = handler;
        this.devConsole = devConsole;
        this.engine = engine;

    }

    public void keyPressed(KeyEvent e) {
        int key = e.getKeyCode();
                if (key == KeyEvent.VK_W||key == KeyEvent.VK_UP) UP=true;
                if (key == KeyEvent.VK_S||key == KeyEvent.VK_DOWN) DOWN=true;
                if (key == KeyEvent.VK_D||key == KeyEvent.VK_RIGHT) RIGHT=true;
                if (key == KeyEvent.VK_A||key == KeyEvent.VK_LEFT) LEFT=true;
                if (key == KeyEvent.VK_F1){
                    devConsole.showConsole();
                }
                if (key == KeyEvent.VK_ESCAPE){
                    engine.setState(Engine.State.Menu);
                }
    }

    public void keyReleased(KeyEvent e){
        int key = e.getKeyCode();
                if(key == KeyEvent.VK_W||key == KeyEvent.VK_UP) UP=false;
                if(key == KeyEvent.VK_S||key == KeyEvent.VK_DOWN) DOWN=false;
                if(key == KeyEvent.VK_D||key == KeyEvent.VK_RIGHT) RIGHT=false;
                if(key == KeyEvent.VK_A||key == KeyEvent.VK_LEFT) LEFT=false;
    }
    public static void velocityUpdate(GraphicObject graphicObject){
        if (DOWN) {
            graphicObject.yVel = 2;
        }
        if (UP) {
            graphicObject.yVel = -2;
        }
        if (RIGHT) {
            graphicObject.xVel = 2;
        }
        if (LEFT) {
            graphicObject.xVel = -2;
        }
        if (!DOWN && !UP) {
            graphicObject.yVel = 0;
        }
        if (!RIGHT && !LEFT) {
            graphicObject.xVel = 0;
        }
    }

}
