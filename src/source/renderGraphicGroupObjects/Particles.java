package source.renderGraphicGroupObjects;

import source.engine.Engine;
import source.engineLogic.GraphicObjectsGroup;
import source.engineLogic.Handler;
import source.enums.GroupObjectType;

import java.awt.*;

public class Particles extends GraphicObjectsGroup{

    public Particles(int objectsQuatity, String name, GroupObjectType groupObjectType, int x[], int y[], int xSize[], int ySize[], String internalName[], Handler handler, Engine engine){
        super(objectsQuatity, name, groupObjectType , x, y, xSize, ySize, internalName, handler, engine);
    }

    public Particles(int objectsQuatity, String name, GroupObjectType groupObjectType ,int x[], int y[], int xSize, int ySize, String internalName, Handler handler, Engine engine) {
        super(objectsQuatity, name, groupObjectType, x, y, xSize, ySize, internalName, handler, engine);
    }

    public Particles(int objectsQuatity, String name, GroupObjectType groupObjectType , int xSize, int ySize, String internalName, Handler handler, Engine engine){
        super(objectsQuatity, name, groupObjectType, xSize, ySize, internalName, handler, engine);
    }

    public void tick(){

    }

    public void render(Graphics g){
        for(int c = 0;c<objectsQuatity;c++){
            g.setColor(Color.GREEN);
            g.fillRect(x[c], y[c], xSize[c], ySize[c]);
        }

    }

}
