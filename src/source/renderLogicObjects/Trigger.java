package source.renderLogicObjects;

import source.engine.Engine;
import source.engineLogic.GraphicObject;
import source.engineLogic.Handler;
import source.engineLogic.LogicObject;
import source.enums.LogicObjectType;
import source.enums.ObjectType;

import java.awt.*;

public class Trigger extends LogicObject{

    private boolean isDetection=false;

    public Trigger(int x, int y, int xSize, int ySize, LogicObjectType logicObjectType, String name, Handler handler, Engine engine, boolean objectInfo){
        super(x, y, xSize, ySize, logicObjectType, name, handler, engine, objectInfo);
    }
    public Trigger(int x, int y, int xSize, int ySize, LogicObjectType logicObjectType, String name, Handler handler, Engine engine){
        super(x, y, xSize, ySize, logicObjectType, name, handler, engine);
    }

    public Rectangle getBounds(){
        return new Rectangle(x, y, xSize, ySize);
    }
    public void render(Graphics g){
        if(engine.getShowLogicObject()){
            if(isDetection) {
                g.setColor(Color.RED);
            }else {
                g.setColor(Color.GREEN);
            }
            //draw square
            g.drawLine(x,y, x,y+ySize);
            g.drawLine(x,y, x+xSize,y);
            g.drawLine(x+xSize,y, x+xSize,y+ySize);
            g.drawLine(x,y+ySize, x+xSize,y+ySize);
            //draw diagonales
            g.drawLine(x, y, x+xSize, y+ySize);
            g.drawLine(x+xSize, y, x, y+ySize);

        }
    }
    public void tick(){
        collision();
    }
    public void collision(){
        for(int i = 0; i < handler.graphicObjects.size(); i++){
            GraphicObject tempobject = handler.graphicObjects.get(i);
            if(tempobject.getObjectType() == ObjectType.Player || tempobject.getObjectType() == ObjectType.Ball){
                isDetection = getBounds().intersects(tempobject.getBounds());
            }
        }
    }
}
