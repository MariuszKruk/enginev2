package source.devTools;

import source.engine.Engine;
import source.engineLogic.Handler;
import source.enums.GroupObjectType;
import source.enums.LogicObjectType;
import source.enums.ObjectType;
import source.otherRender.HUD;
import source.otherRender.geometry3D.DrawCube;
import source.renderGraphicGroupObjects.Particles;
import source.renderGraphicObjects.Ball;
import source.renderGraphicObjects.Coin;
import source.renderGraphicObjects.Particle;
import source.renderGraphicObjects.Player;
import source.renderLogicObjects.Trigger;

import javax.swing.*;
import javax.swing.text.StyledDocument;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.*;
import java.util.List;

public class DevConsole{

    private final List<String> args = new ArrayList<>();
    private final List<String> instructions = new ArrayList<>();
    private final List<String> lastCommands = new ArrayList<>();

    private int logicInt=-1;

    private static JFrame frame;
    private final JTextField input;

    public StyledDocument document;

    final Handler handler;
    final Engine engine;

    public DevConsole(int WIDTH, Handler handler, Engine engine){
        this.engine = engine;
        this.handler = handler;
        try{
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }catch (Exception ex){}


        frame = new JFrame();
        frame.setUndecorated(true);
        frame.setTitle("Console");
        frame.setDefaultCloseOperation(frame.getDefaultCloseOperation());

        JTextPane console = new JTextPane();
        console.setEditable(false);
        console.setFont(new Font("Courier New", Font.PLAIN, 12));
        console.setOpaque(false);

        input = new JTextField();
        input.setEditable(true);
        input.setForeground(Color.WHITE);
        input.setCaretColor(Color.WHITE);
        input.setOpaque(false);
        input.addActionListener(e -> {
            String text = input.getText();

            if(text.length() > 1){
                prepareCommand(text);
            }
        });

        input.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }
            @Override
            public void keyPressed(KeyEvent e) {

                int key = e.getKeyCode();
                if (!lastCommands.isEmpty()) {
                    if (key == KeyEvent.VK_UP || key == KeyEvent.VK_DOWN) {
                        if (key == KeyEvent.VK_UP) {
                            logicInt = logicInt + 1;
                            if (logicInt > lastCommands.size() - 1) {
                                logicInt = lastCommands.size() - 1;
                            }
                        }
                        if (key == KeyEvent.VK_DOWN) {
                            logicInt = logicInt - 1;
                            if (logicInt < 0) {
                                logicInt = 0;
                            }
                        }
                        input.setText(lastCommands.get(logicInt));
                    }
                }
                    if (key == KeyEvent.VK_F1) {
                        showConsole();
                    }
                    if (key == KeyEvent.VK_ENTER) {
                        logicInt = -1;
                    }
                }
            @Override
            public void keyReleased(KeyEvent e) {

            }
        });

        JScrollPane scrollPane = new JScrollPane(console);
        scrollPane.setBorder(null);
        scrollPane.setOpaque(false);
        scrollPane.getViewport().setOpaque(false);
        scrollPane.setEnabled(false);

        frame.add(input, BorderLayout.SOUTH);
        frame.add(console, BorderLayout.CENTER);

        frame.getContentPane().setBackground(new Color(50, 50, 50));

        frame.setSize(WIDTH, 150);
        frame.setLocation(5, 58);

        frame.setResizable(false);
        frame.setVisible(true);
        frame.requestFocus();
        input.requestFocus();

    }
    public void tick(){
        updateWindowPosition();
        execCommand();

    }
    public void showConsole(){
        if(frame.isVisible()){
            frame.setVisible(false);
        }else{
            frame.setVisible(true);
        }
    }
    public void close(){
        frame.dispose();
    }
    public void updateWindowPosition(){
        //if(!isAnimation){
        frame.setLocation(engine.getLocation().x, engine.getLocation().y+engine.getHeigh()+32);
        //     locationG=frame.getLocation();
        // }else{
        //    frame.setLocation(locationG);
        //}
    }

    private synchronized void prepareCommand(String instruction){
        if(instructions.indexOf(instruction)>0){
        }else{
            instructions.add(instruction);
        }
        input.setText("");
    }
    private void execCommand(){
        for(int i = 0; i < instructions.size(); i++){
            try {
            String instruction = instructions.get(i);
            instructions.remove(instruction);
            if(!lastCommands.contains(instruction)){
                lastCommands.add(0, instruction);
            }
            args.clear();
            while(true){
                int read, stop;

                read = instruction.indexOf("-");
                if(read == -1){
                    break;
                }
                stop = instruction.indexOf(";");

                args.add(instruction.substring(read + 1, stop));
                instruction = instruction.replaceFirst(instruction.substring(read, stop + 1), "");
            }
                switch (instruction.substring(0, instruction.indexOf("#"))) {
                    case "Add_Object":
                        addGraphicObjectByType();
                        break;
                    case "Remove_Object_ByName":
                        removeGraphicObjectByName();
                        break;
                    case "Remove_Object_ById":
                        removeGraphicObjectById();
                        break;
                    case "Remove_Object_ByType":
                        removeGraphicObjectByType();
                        break;
                    case "Add_ObjectsGroup":
                        addGraphicObjectGroup();
                        break;
                    case "Remove_ObjectsGroup_ByName":
                        removeGraphicObjectGroupByName();
                        break;
                    case "Remove_ObjectsGroup_ById":
                        removeGraphicObjectGroupById();
                        break;
                    case "Remove_ObjectsGroup_ByType":
                        removeGraphicObjectGroupByType();
                        break;
                    case "Add_LogicObject":
                        addLogicObject();
                        break;
                    case "Remove_LogicObject_ByName":
                        removeLogicObjectByName();
                        break;
                    case "Remove_LogicObject_ByType":
                        removeLogicObjectByType();
                        break;
                    case "Remove_LogicObject_ById":
                        removeLogicObjectById();
                        break;
                    case "List_All_GraphicObjects":
                        listAllGraphicObjects();
                        break;
                    case "List_All_LogicObjects":
                        listAllLogicObjects();
                        break;
                    case "List_All_GraphicObjectsGroups":
                        listAllGraphicObjectsGroups();
                        break;
                    case "List_All_Objects":
                        listAllObjects();
                        break;
                    case "Set_Value":
                        setValue();
                        break;
                    case "SetHeight":
                        setSize();
                        break;
                    case "ShowFPS":
                        showFPS();
                        break;
                    case "Help":
                        printHelp();
                        break;
                    case "Show_Triggers":
                        showTriggers();
                        break;
                    case "Hide_Console":
                        hideConsole();
                        break;
                    case "Set_State":
                        setState();
                        break;
                    case "CleanUp":
                        cleanUp();
                        break;
                    case "Exit":
                        exit();
                        break;
                    default:
                        System.out.println("c");
                }
            }catch(Exception e){
                System.out.println("Something went wrong: check command");
            }
        }
    }

    //COMMANDS
        //OBJECT
    public void addGraphicObjectByType(){
        switch(args.get(0)){
            case "ball":
                handler.addObject(new Ball(
                        Integer.parseInt(args.get(1)),
                        Integer.parseInt(args.get(2)),
                        Integer.parseInt(args.get(3)),
                        Integer.parseInt(args.get(4)),
                        ObjectType.Ball,
                        args.get(5),
                        handler, engine));
                break;
            case "coin":
                handler.addObject(new Coin(
                        ObjectType.Coin,
                        args.get(1),
                        handler, engine));
                break;
            case "particle":
                handler.addObject(new Particle(
                        Integer.parseInt(args.get(1)),
                        Integer.parseInt(args.get(2)),
                        Integer.parseInt(args.get(3)),
                        Integer.parseInt(args.get(4)),
                        ObjectType.Particle,
                        args.get(5),
                        handler, engine));
                break;

            case "player":
                handler.addObject(new Player(
                        Integer.parseInt(args.get(1)),
                        Integer.parseInt(args.get(2)),
                        Integer.parseInt(args.get(3)),
                        Integer.parseInt(args.get(4)),
                        ObjectType.Player,
                        args.get(5),
                        handler, engine));
                break;
            default:
                System.out.println("ERROR: BAD OBJECT ID");
        }
    }
    public void removeGraphicObjectByName(){
        handler.removeObjectByName(args.get(0));
    }
    public void removeGraphicObjectById(){
        handler.removeObjectById(args.get(0));
    }
    public void removeGraphicObjectByType(){
        switch(args.get(0)) {
            case "ball":
                handler.removeObjectByType(ObjectType.Ball);
                break;
            case "coin":
                handler.removeObjectByType(ObjectType.Coin);
                break;
            case "particle":
                handler.removeObjectByType(ObjectType.Particle);
                break;
            case "player":
                handler.removeObjectByType(ObjectType.Player);
                break;
        }
    }
        //OBJECTSGROUP
    public void addGraphicObjectGroup(){
        switch(args.get(0)){
            case "ParticlesGroup":
                handler.addGraphicObjectGroup(new Particles(
                        Integer.parseInt(args.get(1)),
                        args.get(2),
                        GroupObjectType.groupParticles,
                        Integer.parseInt(args.get(3)),
                        Integer.parseInt(args.get(4)),
                        args.get(5), handler, engine
                        ));
        }
    }
    public void removeGraphicObjectGroupByName(){
        handler.removeObjectByName(args.get(0));
    }
    public void removeGraphicObjectGroupByType(){
        switch(args.get(0)) {
            case "ParticlesGroup":
                handler.removeObjectsGroupByType(GroupObjectType.groupParticles);
                break;
        }
    }
    public void removeGraphicObjectGroupById(){
        handler.removeObjectById(args.get(0));
    }
        //LOGICOBJECT
    public void addLogicObject(){
        switch(args.get(0)){
            case "trigger":
               handler.addLogicObject(new Trigger(
                       Integer.parseInt(args.get(2)),
                       Integer.parseInt(args.get(3)),
                       Integer.parseInt(args.get(4)),
                       Integer.parseInt(args.get(5)),
                       LogicObjectType.Trigger,
                       args.get(1),
                       handler,
                       engine
                       ));
        }
    }
    public void removeLogicObjectByName(){
        handler.removeObjectByName(args.get(0));
    }
    public void removeLogicObjectByType(){
        switch(args.get(0)) {
            case "trigger":
                handler.removeObjectsGroupByType(GroupObjectType.groupParticles);
                break;
        }
    }
    public void removeLogicObjectById(){
        handler.removeObjectById(args.get(0));
    }
        //OTHER
    public void setValue(){
        switch(args.get(0)){
            case "HEALTH":
                HUD.setHEALTH(Integer.parseInt(args.get(1)));
                break;
            case "MONEY":
                HUD.setMONEY(Integer.parseInt(args.get(1)));
        }
    }
    public void printHelp(){
        System.out.println("List of commands:");
        System.out.println("---------------------------------------");
        System.out.println("GraphicObjects:");

        System.out.println();

        System.out.println("Add_Object# -object type; -position x; -position y; -size x; -size y; -name;");
        System.out.println("Remove_Object_ById# -id;");
        System.out.println("Remove_Object_ByName# -name;");
        System.out.println("Remove_Object_ByType# -type;");

        System.out.println("---------------------------------------");

        System.out.println("GraphicObjectsGroup:");

        System.out.println();
        System.out.println("Add_ObjectsGroup# -objectgroup type; -qunatity of objects; -group name; -size x; -size y; -name of objects;");
        System.out.println("Remove_ObjectsGroup_ById# -id;");
        System.out.println("Remove_ObjectsGroup_ByName -name;");
        System.out.println("Remove_ObjectsGroup_ByType -type;");

        System.out.println("---------------------------------------");

        System.out.println("LogicObjects:");

        System.out.println();
        System.out.println("Add_LogicObject# -LogicObjectType; -name; -x; -y; -xSize; -ySize;");
        System.out.println("Remove_LogicObject_ById# -id;");
        System.out.println("Remove_LogicObject_ByName -name;");
        System.out.println("Remove_LogicObject_ByType -type;");

        System.out.println("---------------------------------------");

        System.out.println("Other:");

        System.out.println();
        System.out.println("Set_Value# -value name; -value;");
        System.out.println("Set_Size# -size;");
        System.out.println("Show_Triggers#");
        System.out.println("Hide_Console#");
        System.out.println("UnlockConsole#");
        System.out.println("ShowFPS#");
        System.out.println("Exit#");
        System.out.println();
    }
    public void listAllGraphicObjectsGroups(){}
    public void listAllGraphicObjects(){}
    public void listAllLogicObjects(){}
    public void listAllObjects(){
        handler.listAllObjects();
    }
    public void showTriggers(){
        if(engine.getShowLogicObject())
        engine.setShowLogicObject(false);
        else{
            engine.setShowLogicObject(true);
        }
    }
    public void hideConsole(){
        showConsole();
    }
    public void cleanUp(){
        handler.graphicObjects.clear();
        handler.logicObjects.clear();
        handler.graphicObjectsGroups.clear();
    }
    public void setSize(){
        frame.setSize(frame.getWidth(), Integer.parseInt(args.get(0)));
    }
    public void showFPS() {
        engine.switchShowFPS();
    }
    public void setState(){
        switch(args.get(0)){
            case "Menu":
                engine.setState(Engine.State.Menu);
                break;
            case "StandardLoop":
                engine.setState(Engine.State.Work);
                break;
            case "Intro":
                engine.setState(Engine.State.Intro);
                break;
            case "Loading":
                engine.setState(Engine.State.Loading);
                break;
            case "Start":
                engine.setState(Engine.State.Start);
        }
    }
    public void exit(){
        System.exit(9);
    }


    /*
    public void showConsoleAnimation(){
        Point location = Engine.getLocation();
        isAnimation=true;
        frame.setVisible(true);
        for(int i = 0;i<frame.getHeight();i++){
            try {
                Thread.currentThread().sleep(5);
            }catch(Exception e){
                e.printStackTrace();
            }
            frame.setLocation(location.x,location.y+Engine.getHeigh()+32-frame.getHeight()+i);
            locationG=frame.getLocation();
        }
        isAnimation=false;
    }
    public void hideConsoleAnimation(){
        Point location = Engine.getLocation();
        isAnimation=true;
        for(int i = frame.getHeight();i>frame.getHeight();i--){
            try {
                Thread.currentThread().sleep(5);
            }catch(Exception e){
                e.printStackTrace();
            }
            locationG.setLocation(location.x,location.y+Engine.getHeigh()+32-i);
            frame.setLocation(location.x,location.y+Engine.getHeigh()+32-i);
        }
        frame.setVisible(false);
        isAnimation=false;
    }

    static boolean isAnimation=false;
    */
}
