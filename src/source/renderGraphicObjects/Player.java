package source.renderGraphicObjects;


import source.engine.Engine;
import source.engineLogic.GraphicObject;
import source.engineLogic.Handler;
import source.engineLogic.KeyInput;
import source.enums.ObjectType;

import java.awt.*;

public class Player extends GraphicObject {

    public Player(int x, int y, int xSize, int ySize, ObjectType objectType, String name, Handler handler, Engine engine, boolean objectInfo) {
        super(x, y, xSize, ySize, objectType, name, handler, engine, objectInfo);
    }

    public Player(int x, int y, int xSize, int ySize, ObjectType objectType, String name, Handler handler, Engine engine){
        super(x, y, xSize, ySize, objectType, name, handler, engine);
    }

    public Rectangle getBounds() {
        return new Rectangle(x, y, xSize, ySize);
    }

    public void tick() {

        KeyInput.velocityUpdate(this);

        x += xVel;
        y += yVel;


        clamp(x, engine.getWidth() - xSize, 0, y, engine.getHeigh() - ySize, 0);
    }

    public void render(Graphics g){
        Graphics2D g2d = (Graphics2D) g;
        g.setColor(Color.red);
        g.fillRect(x, y, xSize, ySize);
    }

    public void collision(){
        for (int i = 0; i < handler.graphicObjects.size(); i++) {
            GraphicObject tempobject = handler.graphicObjects.get(i);
            if (tempobject.getObjectType() == ObjectType.Player) {
                if (getBounds().intersects(tempobject.getBounds())){
                    //REMOVE HEALTH
                    System.out.println("HIT DETECTED");
                }
            }
        }
    }
}
