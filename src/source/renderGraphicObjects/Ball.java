package source.renderGraphicObjects;

import source.engine.Engine;
import source.engineLogic.GraphicObject;
import source.engineLogic.Handler;
import source.enums.ObjectType;

import java.awt.*;

public class Ball extends GraphicObject{

    public Ball(int x, int y, int xSize, int ySize, ObjectType objectType , String name, Handler handler, Engine engine,boolean objectInfo){
        super(x ,y ,xSize ,ySize, objectType ,name, handler, engine,objectInfo);
        xVel=5;
        yVel=5;
    }

    public Ball(int x, int y, int xSize, int ySize, ObjectType objectType , String name, Handler handler, Engine engine){
        super(x ,y ,xSize ,ySize, objectType, name,handler, engine);
        xVel=5;
        yVel=5;
    }

    public Rectangle getBounds(){
        return new Rectangle(x,y,xSize,ySize);
    }

    public void tick(){
        x+=xVel;
        y+=yVel;

        if(y <= 0 || y >= engine.getHeigh()-ySize) yVel*=-1;
        if(x <= 0 || x >= engine.getWidth()-xSize) xVel*=-1;
        // movment();
    }
    public void render(Graphics g){
            g.setColor(Color.blue);
            g.fillRect(x, y, xSize, ySize);
    }
}
