package source.renderGraphicObjects;

import source.engine.Engine;
import source.engineLogic.GraphicObject;
import source.engineLogic.Handler;
import source.enums.ObjectType;
import source.otherRender.HUD;

import java.awt.*;
import java.util.Random;

public class Coin extends GraphicObject{

    int animationduration=0;
    int animationduration0=0;

    public Coin(ObjectType objectType , String name, Handler handler ,Engine engine ,boolean objectInfo){
        super(objectType ,name, handler, engine, objectInfo);
        xSize=9;
        ySize=9;
        randomspawn();
    }

    public Coin(ObjectType objectType ,String name, Handler handler, Engine engine){
        super(objectType ,name ,handler, engine);
        xSize=9;
        ySize=9;
        randomspawn();
    }

    public Rectangle getBounds(){
        return new Rectangle(x,y,xSize,ySize);
    }

    public void tick(){
        collision();
    }

    public void render(Graphics g){
        animationduration++;
        g.setColor(Color.YELLOW);
        if (animationduration > 1500) {
            g.fillRect(x, y - 3, xSize, ySize);
            animationduration0++;
            if (animationduration0 > 420) {
                animationduration = 0;
                animationduration0 = 0;
            }
        }else{
            g.fillRect(x, y, xSize, ySize);
        }
    }

    public void randomspawn(){
        Random random = new Random();
        y=random.nextInt(engine.getHeigh());
        x=random.nextInt(engine.getWidth());
    }
    private void collision(){
        for(int i = 0; i< handler.graphicObjects.size();i++){
            GraphicObject tempobject = handler.graphicObjects.get(i);
            if(tempobject.getObjectType()==objectType.Player){
                if(getBounds().intersects(tempobject.getBounds())){
                    handler.removeObject(this);
                    HUD.MONEY++;
                }
            }
        }
    }
}
