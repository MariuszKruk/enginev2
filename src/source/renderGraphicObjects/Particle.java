package source.renderGraphicObjects;

import source.engine.Engine;
import source.engineLogic.GraphicObject;
import source.engineLogic.Handler;
import source.enums.ObjectType;

import java.awt.*;

/**
 * Created by Kuba on 2018-04-25.
 */

public class Particle extends GraphicObject{

    public Particle(int x, int y, int xSize, int ySize, ObjectType objectType, String name, Handler handler, Engine engine, boolean objectInfo) {
        super(x, y, xSize, ySize, objectType, name, handler, engine,objectInfo);
    }

    public Particle(int x, int y, int xSize, int ySize, ObjectType objectType, String name, Handler handler, Engine engine) {
        super(x, y, xSize, ySize, objectType, name, handler, engine);
    }
    public void tick(){

    }
    public void render(Graphics g){
        g.setColor(Color.GREEN);
        g.fillRect(x, y, xSize, ySize);

    }
    public Rectangle getBounds(){
        return new Rectangle(x, y, xSize, ySize);
    }

}
