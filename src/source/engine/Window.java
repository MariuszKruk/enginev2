package source.engine;

import source.engineLogic.RenderManagener;

import javax.swing.*;
import java.awt.*;

public class Window extends Canvas{

    private final JFrame frame;

    public Window(int width, int height, String title, Engine engine, RenderManagener renderManagener, boolean fullscreen){
        frame = new JFrame(title);

        if(fullscreen){
            frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
            frame.setUndecorated(true);
            frame.setVisible(true);
        }
        else {
            frame.setPreferredSize(new Dimension(width, height));
            frame.setMaximumSize(new Dimension(width, height));
            frame.setMinimumSize(new Dimension(width, height));
        }

        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.add(renderManagener);
        frame.setVisible(true);
        frame.requestFocus();
        engine.start();
    }
    public Point getLocation(){
        return frame.getLocation();
    }
    public void close(){
        frame.dispose();
    }
}