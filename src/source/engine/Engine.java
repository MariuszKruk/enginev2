package source.engine;

import source.devTools.DIDevConsole;
import source.devTools.DevConsole;
import source.engineLogic.Handler;
import source.engineLogic.RenderManagener;
import source.engineLogic.Tasks;
import source.otherRender.scenes.*;
import source.otherRender.scenes.Menu;

import java.awt.*;


public class Engine implements Runnable{

    /**
     * Major class wherein is located main loop which call to render and tick
     */

    private final String TITLE="MBKO";
    private static final int WIDTH=100;
    private static final int HEIGHT=56;
    private static final int SCALE=10;
    private static final int TOPBORDER=28;
    private static final int BORDER=6;
    private boolean fullscreen = false;
    private int DELAY;

    private final DevConsole devConsole;
    private final RenderManagener renderManagener;
    private final Window window;
    private final Handler handler;
    private final Tasks tasks;
    private Thread thread;
    private Thread monitor;

    //SCENES
    private final Options options;
    private final Loading loading;
    private final Start start;
    private final Intro intro;
    private final Menu menu;


    /**
     *
     */
    private boolean running = false;
    private boolean showFPS = false;
    private boolean showlogicobject = false;
    private State state = State.Menu;

    private int seconds = 1;
    private int framesum = 0;

    public enum State{
        Work,
        Loading,
        Start,
        Intro,
        Options,
        Menu
    }

    public Engine(){
        options = new Options(this);
        loading = new Loading();
        intro = new Intro();
        menu = new Menu(this);
        start = new Start();
        handler = new Handler();
        tasks = new Tasks(handler);
        devConsole = new DevConsole(WIDTH*SCALE, handler, this);
        renderManagener = new RenderManagener(handler, devConsole, this,WIDTH*SCALE,HEIGHT*SCALE);
        window = new Window(WIDTH*SCALE,HEIGHT*SCALE, TITLE, this, renderManagener, fullscreen);
        System.out.println(this);

        monitor = new Thread(new Monitor(this) );
        monitor.start();
    }

    public synchronized void start(){
        thread = new Thread(this);
        thread.start();

        running = true;
    }

    public synchronized void stop(){
        thread = new Thread(this);
        thread.stop();

        running = false;
    }

    public void run(){
        engineLoop();
    }

    /**
     * Engine LOOP
     */
    private void engineLoop(){
        int frame=0;
        double delta = 0;
        double ticks = 240;
        long lastTime = System.nanoTime();
        double nanoseconds = 1000000000/ticks;
        long timer = System.currentTimeMillis();
        while(running){
            long now = System.nanoTime();
            delta += (now - lastTime) / nanoseconds;
            lastTime = now;
            while (delta >= 1) {
                sceneTick();
                delta--;
            }
            if (running){
                renderManagener.render(state);
                try{
                    Thread.currentThread().sleep(DELAY);
                }catch(Exception e){
                    e.printStackTrace();
                }
                frame++;
            }
            if(System.currentTimeMillis() - timer > 1000){
                timer += 1000;
                fpsStabilizer(frame);
                frame = 0;
            }
        }
        stop();
    }

    private void tick(){
        tasks.tick();
        renderManagener.tick();
        handler.tick();
    }
    private void sceneTick(){
        switch(state){
            case Work:
                tick();
                break;
            case Loading:
            case Intro:
            case Start:
            case Menu:
                menu.tick();
                break;
        }
        devConsole.tick();
    }
    private void fpsStabilizer(int fps){
        if(fps>10000){
            DELAY=8;
        }
        if(fps>9000&&fps<10000){
            DELAY=7;
        }
        if(fps>8000&&fps<9000){
            DELAY=6;
        }
        if(fps>7000&&fps<8000){
            DELAY=5;
        }
        if(fps>6000&&fps<7000){
            DELAY=4;
        }
        if(fps>2000&&fps<6000){
            DELAY=3;
        }
        if(fps>1000&&fps<2000){
            DELAY=2;
        }
        if(fps<250){
            DELAY=0;
        }
    }
    public int getWidth(){
        return WIDTH*SCALE-BORDER;
    }
    public int getHeigh(){
        return HEIGHT*SCALE-TOPBORDER;

    }
    public boolean getShowLogicObject(){
        return showlogicobject;
    }
    public void setShowLogicObject(boolean showlogicobject){
        this.showlogicobject = showlogicobject;
    }
    public Point getLocation(){
        return window.getLocation();
    }
    private void showFPS(int frames){
        if(showFPS){
            System.out.println("FPS:[" + frames + "]");
            framesum = framesum + frames;
            System.out.println("Average FPS:[" + framesum / seconds + "]");
        }
    }
    public void switchShowFPS(){
        if(showFPS){
            showFPS = false;
        }else{
            showFPS = true;
        }
    }
    public void injectScenesDependections(DependencyInjectionScenes dependencyInjectionScenes){
        dependencyInjectionScenes.setMenu(menu);
        dependencyInjectionScenes.setLoading(loading);
        dependencyInjectionScenes.setStart(start);
        dependencyInjectionScenes.setIntro(intro);
        dependencyInjectionScenes.setOptions(options);
    }
    public void injectDevConsole(DIDevConsole diDevConsole){
        diDevConsole.setDevConsole(devConsole);
    }
    public void injectWindow(DIWindow diWindow){
        diWindow.setWindow(window);
    }
    public void setState(State state){
        this.state = state;
    }
    public int getScale(){
        return SCALE;
    }
    public String getTitle(){
        return TITLE;
    }
    public void suspend(){
        running = false;
    }
    public State getState(){
        return state;
    }
}
